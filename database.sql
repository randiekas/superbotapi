/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : chatbot

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 21/10/2021 09:55:43
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for akun
-- ----------------------------
DROP TABLE IF EXISTS `akun`;
CREATE TABLE `akun` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `tipe` enum('superadmin','biasa') CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `dibuat` datetime DEFAULT NULL,
  `diubah` datetime DEFAULT NULL,
  `dihapus` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of akun
-- ----------------------------
BEGIN;
INSERT INTO `akun` VALUES (1, 'superadmin', 'abcd1234', 'superadmin', NULL, '2021-10-11 15:08:11', NULL, NULL);
INSERT INTO `akun` VALUES (2, 'guruindonesia@gmail.com', 'abcd1234', 'biasa', 'Bahasa Indonesia', NULL, '2021-10-15 07:04:18', NULL);
INSERT INTO `akun` VALUES (7, 'gurumtk@gmail.com', 'abcd1234', 'biasa', 'Guru Indonesia', NULL, '2021-10-15 07:07:40', NULL);
INSERT INTO `akun` VALUES (8, 'adminsekolah@gmail.com', 'abcd1234', 'biasa', 'Admin jadwal', NULL, '2021-10-18 08:41:38', NULL);
INSERT INTO `akun` VALUES (9, 'krisna', 'abcd1234', 'biasa', 'guru it', NULL, '2021-10-19 05:07:35', NULL);
INSERT INTO `akun` VALUES (10, 'yudha', 'abcd1234', 'biasa', 'guru sampingan it', NULL, '2021-10-19 05:07:58', NULL);
COMMIT;

-- ----------------------------
-- Table structure for alur
-- ----------------------------
DROP TABLE IF EXISTS `alur`;
CREATE TABLE `alur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_akun` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `parentId` char(9) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT '',
  `katakunci` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `balasan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `fallback` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `mode` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `opsi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `dibuat` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  FULLTEXT KEY `alur_katakunci` (`katakunci`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of alur
-- ----------------------------
BEGIN;
INSERT INTO `alur` VALUES (1, NULL, 0, 'Halo', '', 'hallo,hai,mau tanya', '[\"Selamat datang di guru asisten SCOLA LMS\",\"Apa yang bisa kami bantu?\"]', 'belum disetting', 'opsi', '[{\"label\":\"hasil belajar\",\"aksi\":\"link\",\"tujuan\":\"https://bola.kompas.com\"},{\"label\":\"jadwal\",\"aksi\":\"flowchat\",\"tujuan\":\"39\"},{\"label\":\"belajar\",\"aksi\":\"flowchat\",\"tujuan\":\"40\"},{\"label\":\"keuangan\",\"aksi\":\"flowchat\",\"tujuan\":\"41\"}]', '2021-09-30 13:41:09');
INSERT INTO `alur` VALUES (39, 1, 1, 'Jadwal belajar', '1', 'jadwal,jadwal belajar', '[\"Berikut jadwal untuk anak anda\"]', '', 'teks', '[{\"aksi\":\"link\",\"label\":\"jadwal kelas 10\"},{\"label\":\"jadwal kelas 12\"}]', '2021-10-19 07:39:08');
INSERT INTO `alur` VALUES (40, 1, 1, 'Belajar', '1', 'belajar,mau belajar', '[\"kamu mau belajar apa hari ini ?\"]', '', 'teks', '[{\"label\":\"Matematika\",\"aksi\":\"flowchat\",\"tujuan\":\"42\"},{\"label\":\"Bahasa Indonesia\",\"aksi\":\"link\"},{\"label\":\"Bahasa Inggris\",\"aksi\":\"link\"}]', '2021-10-19 07:40:08');
INSERT INTO `alur` VALUES (41, 1, 1, 'Keuangan', '1', 'keuangan', '[]', '', 'teks', '[{\"label\":\"klik link disini\",\"aksi\":\"link\",\"tujuan\":\"https://scola.id/\"}]', '2021-10-19 07:42:09');
INSERT INTO `alur` VALUES (42, 9, 1, 'Matematika', '1', 'belajar matematika,belajar mtk,matematika', '[\"Kamu mau apa hari ini ?\"]', '', 'teks', '[{\"label\":\"Lihat Materi\",\"aksi\":\"link\",\"tujuan\":\"https://scola.id/\"},{\"label\":\"Latihan Soal 1\",\"aksi\":\"flowchat\",\"tujuan\":\"48\"}]', '2021-10-19 07:46:14');
INSERT INTO `alur` VALUES (47, 9, 11, 'Materi', '42', 'materi matematika', '[\"Silahkan pilih materi \"]', '', 'teks', '[{\"label\":\"Materi kelas 10\",\"aksi\":\"link\",\"tujuan\":\"https://scola.id\"},{\"label\":\"Materi kelas 11\",\"aksi\":\"link\",\"tujuan\":\"https://scola.id\"}]', '2021-10-19 07:55:05');
INSERT INTO `alur` VALUES (48, 9, 11, 'Latihan Soal', '42', 'latihan soal matematika', '[\"sebuah kereta berkecepatan x , berapakah y nya ?\"]', '', 'teks', '[{\"label\":\"langsung jawab\",\"aksi\":\"link\"},{\"label\":\"hint pengerjaan\",\"aksi\":\"link\"}]', '2021-10-19 07:57:31');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
