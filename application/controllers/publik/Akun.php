<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akun extends CI_Controller {
	public function __construct(){
		parent::__construct();
		header( 'Access-Control-Allow-Origin: *' );
		if ( $_SERVER[ 'REQUEST_METHOD' ] == "OPTIONS" )
		{
			header( 'Access-Control-Allow-Credentials: true' );
			header( 'Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS' );
			header( 'Access-Control-Allow-Headers: ACCEPT, ORIGIN, X-REQUESTED-WITH, CONTENT-TYPE, AUTHORIZATION' );
			header( 'Access-Control-Max-Age: 86400' );
			header( 'Content-Length: 0' );
			header( 'Content-Type: text/plain' );
			exit ;
		}
	}

	public function masuk(){
		$post 					= (Object) post_data();
		$response["status"]		= true;
		$response["message"]	= "";

		$this->db->where("email", $post->email);
		$this->db->where("password", $post->password);
		$cek					= $this->db->get("akun");

		if($cek->num_rows()>0){
			$akun				= $cek->last_row();

			$jwt["id_akun"]		= $akun->id;
			$jwt["tipe"]		= $akun->tipe;
			$jwt["kadaluarsa"]	= date('Y-m-d H:i:s', strtotime('+1 day'));
			$akun->token		= jwt::encode($jwt, $this->config->item("jwt_key"));
			$response["data"]	= $akun;
		}else{
			$response["status"]	= false;
			$response["message"]= "Username dan password anda salah";
		}

		json($response);
	}

	public function masukGoogle(){
		// setdata
		$data			= [];
		$post 			= json_decode($this->security->xss_clean($this->input->raw_input_stream));

		$crl 			= curl_init("https://www.googleapis.com/oauth2/v3/userinfo");
		$headr 			= array();
		$headr[] 		= 'Authorization: '.$post->token;
		curl_setopt($crl, CURLOPT_HTTPHEADER,$headr);
		curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
		$google			= json_decode(curl_exec($crl));
		
		// set reponse
		$response["status"]		= !isset($google->error);
		$response["message"]	= !$response["status"]?"Unauthorized":"";
		// $response["data"]		= $google;
		
		$this->db->select("id");
		$this->db->where("email", $google->email);
		$this->db->where("tipe", $post->tipe);
		$akun		= $this->db->get("akun");
		
		// set data
		$data["nama"]			= $google->name;
		$data["gambar"]			= $google->picture;
		$data["terakhir_masuk"]	= date("Y-m-d H:i:s");
		$data["email"]			= $google->email;
		$data["sub"]			= $google->sub;

		$jwt["tipe"]			= $post->tipe;
		// jika akun nya ada
		if($akun->num_rows()>0){
			$akun	= $akun->last_row();
			$this->db->where("id", $akun->id);
			$this->db->update("akun", $data);
			$jwt["id_akun"]		= $akun->id;
			
			$response["data"]	= jwt::encode($jwt, $this->config->item("jwt_key"));
		}else{
			if($post->tipe=="biasa"){
				$data["tipe"]	= "biasa";
				$data["dibuat"]	= date("Y-m-d H:i:s");
				// insert tabel akun
				$this->db->insert("akun", $data);
				$jwt["id_akun"]			= $this->db->insert_id();

				$response["data"]		= jwt::encode($jwt, $this->config->item("jwt_key"));
			}else{
				$response["status"]		= false;
			$response["message"]	= "email ini tidak terdaftar";
			}
		}
		
		json($response);
	}

	public function jwt(){
		
		$response["status"]		= true;
		$response["message"]	= "";
		$response["data"]		= jwt::decode($_GET['token'], $this->config->item("jwt_key"), true);

		json($response);
	}

	public function status(){
		
		$response["status"]		= true;
		$response["message"]	= "";
		$response["data"]		= jwt();

		json($response);
	}
}
