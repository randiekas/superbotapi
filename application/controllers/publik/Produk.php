<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {
	public function __construct(){
		parent::__construct();
		header( 'Access-Control-Allow-Origin: *' );
		if ( $_SERVER[ 'REQUEST_METHOD' ] == "OPTIONS" )
		{
			header( 'Access-Control-Allow-Credentials: true' );
			header( 'Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS' );
			header( 'Access-Control-Allow-Headers: ACCEPT, ORIGIN, X-REQUESTED-WITH, CONTENT-TYPE, AUTHORIZATION' );
			header( 'Access-Control-Max-Age: 86400' );
			header( 'Content-Length: 0' );
			header( 'Content-Type: text/plain' );
			exit ;
		}
	}
	
	public function index(){
		$response["status"]		= true;
		$response["message"]	= "";

		$response["data"]["terlaris"]	= $this->db
				->query("
					select * 
					from produk 
					order by terjual desc 
					limit 0,6
				")
				->result();

		$response["data"]["rekomendasi"]= $this->db
				->query("
					select produk.* 
					from produk_rekomendasi
					left join produk
						on produk.id = produk_rekomendasi.id_produk
				")
				->result();

		json($response);
	}

	public function cari(){
		$response["status"]		= true;
		$response["message"]	= "";
		
		$keyword				= $this->input->get("keyword");
		$kategori				= $this->input->get("kategori");
		$sortir					= $this->input->get("sortir");
		$hargaMinimal			= $this->input->get("hargaMinimal");
		$hargaMaksimal			= $this->input->get("hargaMaksimal");

		$this->db->like('nama', $keyword, 'after');

		if($kategori!="semua"){
			$this->db->where("id_kategori", $kategori);
		}

		if($sortir=="terbaru"){
			$this->db->order_by("dibuat", "desc");
		}else if($sortir=="termurah"){
			$this->db->order_by("harga", "asc");
		}else if($sortir=="termahal"){
			$this->db->order_by("harga", "desc");
		}else if($sortir=="terlaris"){
			$this->db->order_by("terjual", "desc");
		}

		if($hargaMaksimal!=0){
			$this->db->where('harga >=', $hargaMinimal);
			$this->db->where('harga <=', $hargaMaksimal);
		}

		
		
		$response["data"]		= $this->db->get("produk")->result();

		json($response);
	}
	public function detil($id){
		$response["status"]		= true;
		$response["message"]	= "";

		$response["data"]		= $this->db
				->query("
					select * 
					from produk 
					where id='".$id."'
				")
				->last_row();
		
		if($response["data"]){
			$response["data"]->toko	= $this->db
				->query("
					select * 
					from toko
					where id='".$response["data"]->id_toko."' 
				")
				->last_row();

			$this->tambahLog($id);
		}

		json($response);
	}

	public function toko($id_toko){
		$response["status"]		= true;
		$response["message"]	= "";
		

		$sortir					= $this->input->get("sortir");
		
		$this->db->where('id_toko', $id_toko);

		if($sortir=="terbaru"){
			$this->db->order_by("dibuat", "desc");
		}else if($sortir=="termurah"){
			$this->db->order_by("harga", "asc");
		}else if($sortir=="termahal"){
			$this->db->order_by("harga", "desc");
		}else if($sortir=="terlaris"){
			$this->db->order_by("terjual", "desc");
		}
		
		$response["data"]		= $this->db->get("produk")->result();

		json($response);
	}

	public function tambahLog($id){
		$this->db->query("update produk set dilihat = dilihat+1 where id= ".$id);

		$data["id_produk"]		= $id;
		$data["ip"]				= $_SERVER['REMOTE_ADDR'];
		$data["dikunjungi"]		= date("Y-m-d H:i:s");

		$this->db->query("update produk set dilihat = dilihat+1 where id= ".$id);
		$this->db->insert("pengunjung", $data);
	}
	
}
