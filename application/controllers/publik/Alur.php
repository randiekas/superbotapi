<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alur extends CI_Controller {
	public function __construct(){
		parent::__construct();
		header( 'Access-Control-Allow-Origin: *' );
		if ( $_SERVER[ 'REQUEST_METHOD' ] == "OPTIONS" )
		{
			header( 'Access-Control-Allow-Credentials: true' );
			header( 'Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS' );
			header( 'Access-Control-Allow-Headers: ACCEPT, ORIGIN, X-REQUESTED-WITH, CONTENT-TYPE, AUTHORIZATION' );
			header( 'Access-Control-Max-Age: 86400' );
			header( 'Content-Length: 0' );
			header( 'Content-Type: text/plain' );
			exit ;
		}
	}

	public function index(){
		// setdata
		$response["status"]		= true;
		$response["message"]	= "";

		$post 					= (Object) post_data();

		$this->db->select("balasan, mode, opsi");

		if(isset($post->aksi)){
			$this->db->where("id", $post->tujuan);
		}else{
			$this->db->like("katakunci", $post->katakunci);
		}
		
		$result					= $this->db->get("alur", 1, 0);
		if($result->num_rows()==0){
			$this->db->where('id', '1');
			$this->db->select("fallback");
			$result							= $this->db->get("alur")->last_row();
			$response["data"]["balasan"]	= json_encode(array($result->fallback));
			$response["data"]["mode"]		= "teks";
			$response["data"]["opsi"]		= json_encode(array());
		}else{
			$response["data"]		= $result->last_row();
		}
		json($response);
	}

}
