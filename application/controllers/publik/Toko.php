<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Toko extends CI_Controller {
	public function __construct(){
		parent::__construct();
		header( 'Access-Control-Allow-Origin: *' );
		if ( $_SERVER[ 'REQUEST_METHOD' ] == "OPTIONS" )
		{
			header( 'Access-Control-Allow-Credentials: true' );
			header( 'Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS' );
			header( 'Access-Control-Allow-Headers: ACCEPT, ORIGIN, X-REQUESTED-WITH, CONTENT-TYPE, AUTHORIZATION' );
			header( 'Access-Control-Max-Age: 86400' );
			header( 'Content-Length: 0' );
			header( 'Content-Type: text/plain' );
			exit ;
		}
	}

	public function cari(){
		$response["status"]		= true;
		$response["message"]	= "";
		
		$keyword				= $this->input->get("keyword");
		

		$response["data"]		= $this->db
				->query("
					select 
						* 
					from 
						toko 
					where 
						nama like '".$keyword."%'
				")
				->result();

		json($response);
	}
	
	public function detil($id){
		$response["status"]		= true;
		$response["message"]	= "";

		$response["data"]		= $this->db
				->query("
					select 
						* 
					from 
						toko 
					where id='".$id."'
				")
				->last_row();

		json($response);
	}
}
