<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Toko extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->jwt				= cek_akses();
	}

	public function index(){

		$response["status"]		= true;
		$response["message"]	= "";

		$id_toko				= $this->db->query("select id from toko where id_akun = ".$this->jwt->id_akun." limit 0,1")->last_row();
		
		if(@$id_toko){
			$id_toko			= $id_toko->id;
			$response["data"]	= $this->db
					->query("
						select 
							*
						from toko
						where id = ".$id_toko."
					")
					->last_row();
		}else{
			$response["status"]		= false;
			$response["data"]		= false;
		}
		json($response);
	}

	public function simpan(){
		$response["status"]		= true;
		$response["message"]	= "";
		$post 					= post_data();

		$id_toko				= $this->db->query("select id from toko where id_akun = ".$this->jwt->id_akun." limit 0,1");
		$post->id_akun			= $this->jwt->id_akun;
		
		if($id_toko->num_rows()==0){
			
			$post->disetujui	= 0;
			$response["status"]	= $this->db->insert("toko", $post);

		}else{

			$post->disetujui	= 0;
			$id_toko			= $id_toko->last_row()->id;
			$this->db->where("id", $id_toko);
			$response["status"]	= $this->db->update("toko", $post);

		}

		
		json($response);
	}
}
