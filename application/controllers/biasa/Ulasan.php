<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ulasan extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->jwt				= cek_akses();
	}

	public function tambah(){

		$response["status"]		= true;
		$response["message"]	= "";
		$post 					= post_data();

		$data["id_akun"]		= $this->jwt->id_akun;
		$data["id_produk"]		= $post->id_produk;
		$data["nilai"]			= $post->nilai;
		$data["ulasan"]	= $post->ulasan;
		$data["dibuat"]	= date("Y-m-d H:i:s");

		$this->db->select("id");
		$this->db->where("id_produk", $data["id_produk"]);
		$this->db->where("id_akun", $data["id_akun"]);
		if($this->db->get("produk_ulasan")->num_rows()>0){
			$response["status"]		= false;
		}else{
			$response["status"]		= $this->db->insert("produk_ulasan", $data);
			$this->db->query("
				update 
					produk
				set 
					rating = (select round(avg(nilai)) from produk_ulasan where id_produk=".$data["id_produk"].")
				where id = ".$data["id_produk"]."
			");
		}
		$response["data"]		= "";
		json($response);
	}

}
