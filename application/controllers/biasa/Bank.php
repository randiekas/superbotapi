<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->jwt				= cek_akses();
	}

	public function index(){

		$response["status"]		= true;
		$response["message"]	= "";

		$response["data"]		= $this->db
		->query("
			select * 
			from bank
		")
		->result();
		json($response);
	}
}
