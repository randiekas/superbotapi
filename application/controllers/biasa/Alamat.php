<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alamat extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->jwt				= cek_akses();
	}
	
	public function provinsi(){

		$response["status"]		= true;
		$response["message"]	= "";


		// echo $response;
		$response["data"]		= $this->db
		->query("
			select * 
			from provinsi
		")->result();

		json($response);
	}

	public function kota($id_provinsi){

		$response["status"]		= true;
		$response["message"]	= "";

		// echo $response;
		$response["data"]		= $this->db
		->query("
			select * 
			from kota 
			where id_provinsi = ".$id_provinsi."
		")->result();

		json($response);
	}

	public function biaya($asal, $tujuan, $berat){

		$response["status"]		= true;
		$response["message"]	= "";
		
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, 'https://api.rajaongkir.com/starter/cost');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "origin=".$asal."&destination=".$tujuan."&weight=".$berat."&courier=jne");

		$headers = array();
		$headers[] = 'Content-Type: application/x-www-form-urlencoded';
		$headers[] = 'Key: c902ba12ccba9f6eb6f28b43a2cc49ee';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close($ch);

		// echo $response;
		$response["data"]		= json_decode($result)->rajaongkir->results;

		json($response);
	}
}
