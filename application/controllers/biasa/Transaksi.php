<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->jwt				= cek_akses();
	}

	public function index(){

		$response["status"]		= true;
		$response["message"]	= "";

		$id_toko				= $this->db->query("select id from toko where id_akun = ".$this->jwt->id_akun." limit 0,1")->last_row()->id;
		
		$response["data"]		= $this->db
				->query("
					select 
						akun.nama,
						transaksi.*,
						provinsi.nama as provinsi,
						kota.nama as kota
					from transaksi
					left join provinsi
						on provinsi.id 		= transaksi.id_provinsi
					left join kota
						on kota.id 			= transaksi.id_kota
					left join akun
						on akun.id			= transaksi.id_akun
					where id_toko = ".$id_toko."
				")
				->result();
		json($response);
	}

	public function tambah(){

		$response["status"]		= true;
		$response["message"]	= "";
		$post 					= post_data();

		$transaksi["id_toko"]	= $post->id_toko;
		$transaksi["id_akun"]	= $this->jwt->id_akun;
		$transaksi["dibuat"]	= date("Y-m-d H:i:s");
		$transaksi["id_provinsi"]	= $post->provinsi;
		$transaksi["id_kota"]	= $post->kota;
		$transaksi["alamat"]	= $post->alamat;
		$transaksi["layanan_pengiriman"]= $post->layanan_pengiriman;
		$transaksi["telepon_penerima"]	= $post->telepon_penerima;
		$transaksi["bank_transfer"]	= $post->bank_transfer;
		$transaksi["total_berat"]	= $post->total_berat;
		$transaksi["total_biaya_pengiriman"]	= $post->total_biaya_pengiriman;
		$transaksi["total_harga"]	= $post->total_harga;
		$transaksi["status"]	= "menunggu_pembayaran";
		$response["status"]		= $this->db->insert("transaksi", $transaksi);

		$id_transaksi			= $this->db->insert_id();
		foreach($post->detil as $row){
			$detil					= array();
			$detil["id_transaksi"]	= $id_transaksi;
			$detil["id_produk"]		= $row->id_produk;
			$detil["varian"]		= $row->varian;
			$detil["jumlah"]		= $row->jumlah;
			$detil["berat"]			= $row->berat;
			$detil["harga"]			= $row->harga;
			$this->db->insert("transaksi_detil", $detil);
		}
		
		$response["data"]		= "";
		json($response);
	}

	public function buktiPembayaran(){

		$response["status"]		= true;
		$response["message"]	= "";
		$post 					= post_data();

		$data["bukti_pembayaran"]	= $post->bukti_pembayaran;
		$data["status"]			= "menunggu_konfirmasi_penjual";
		$this->db->where("id", $post->id);
		$response["status"]		= $this->db->update("transaksi", $data);
		json($response);
		
	}

	public function setStatus(){

		$response["status"]		= true;
		$response["message"]	= "";
		$post 					= post_data();

		$data["status"]			= $post->status;
		$this->db->where("id", $post->id);

		$response["status"]		= $this->db->update("transaksi", $data);
		json($response);
		
	}

	public function riwayat(){

		$response["status"]		= true;
		$response["message"]	= "";

		$response["data"]		= $this->db
				->query("
					select 
						transaksi.*,
						provinsi.nama as provinsi,
						kota.nama as kota
					from transaksi
					left join provinsi
						on provinsi.id 		= transaksi.id_provinsi
					left join kota
						on kota.id 			= transaksi.id_kota
					where id_akun = ".$this->jwt->id_akun."
				")
				->result();
		json($response);
	}

	public function detil( $id_transaksi ){

		$response["status"]		= true;
		$response["message"]	= "";

		$data					= $this->db
				->query("
					select transaksi_detil.* 
					from transaksi_detil
					left join transaksi
						on transaksi.id = transaksi_detil.id_transaksi
					where 
						id_akun = ".$this->jwt->id_akun."
						and id_transaksi = ".$id_transaksi."
				")
				->result();
		foreach($data as $row){
			$detil					= $row;
			$detil->produk			= $this->db
				->query("
					select * 
					from produk 
					where id='".$row->id_produk."'
				")
				->last_row();
		}
		$response["data"]			= $data;
		json($response);
	}
}
