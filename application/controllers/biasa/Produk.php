<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->jwt				= cek_akses();
	}
	
	public function index(){
		$response["status"]		= true;
		$response["message"]	= "";

		$kategori				= $this->input->get("kategori");

		$response["data"]		= $this->db
				->query("
					select * 
					from produk 
					where 
						id_toko	= (select id from toko where id_akun = ".$this->jwt->id_akun." limit 0,1)
				")
				->result();
		json($response);
	}

	public function hapus($id=0){
		$response["status"]		= true;
		$response["message"]	= "";

		$response["status"]		= $this->db
				->query("
					delete
					from produk
					where 
						id_toko	= (select id from toko where id_akun = ".$this->jwt->id_akun." limit 0,1)
					and
						id = ".$id."
				");
		json($response);
	}

	public function tambah($id_toko=0){
		$response["status"]		= true;
		$response["message"]	= "";

		$post 					= post_data();
		
		$id_toko				= $this->db->query("select id from toko where id_akun = ".$this->jwt->id_akun." limit 0,1")->last_row()->id;
		$data["id_toko"]		= $id_toko;
		$data["id_kategori"]	= $post->id_kategori;
		$data["gambar"]			= json_encode($post->gambar);
		$data["nama"]			= $post->nama;
		$data["stok"]			= $post->stok;
		$data["rating"]			= 0;
		$data["terjual"]		= 0;
		$data["dilihat"]		= 0;
		$data["deskripsi"]		= $post->deskripsi;
		$data["varian"]			= json_encode($post->varian);
		$data["harga_varian"]	= json_encode($post->harga_varian);
		$data["berat_varian"]	= json_encode($post->berat_varian);
		$data["harga"]			= $post->harga_varian[0];

		if(@$post->id){
			$this->db->where("id", $post->id);
			$response["status"]		= $this->db->update("produk", $data);
		}else{
			$response["status"]		= $this->db->insert("produk", $data);
		}
		
		json($response);
	}

}
