<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
	public function __construct(){
		header( 'Access-Control-Allow-Origin: *' );
		if ( $_SERVER[ 'REQUEST_METHOD' ] == "OPTIONS" )
		{
			log_message( 'debug', 'Configure webserver to handle OPTIONS-request without invoking this script' );
			header( 'Access-Control-Allow-Credentials: true' );
			header( 'Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS' );
			header( 'Access-Control-Allow-Headers: ACCEPT, ORIGIN, X-REQUESTED-WITH, CONTENT-TYPE, AUTHORIZATION' );
			header( 'Access-Control-Max-Age: 86400' );
			header( 'Content-Length: 0' );
			header( 'Content-Type: text/plain' );
			exit ;
		}
		parent::__construct();
	}
	public function upload($folder=""){
		$config = array(
            'upload_path'   => './uploads/'.$folder,
            'allowed_types' => '*',
        );

        $this->load->library('upload', $config);
		$file				= "";

		if (!is_dir('uploads/' . $folder))
		{
			mkdir('./uploads/' . $folder, 0777, true);
		}
		
		if ($this->upload->do_upload('files')) {
			$response["status"]			= true;
			$response["message"]		= "";	
			$file = "/uploads/".$folder."/".$this->upload->data()["file_name"];
		}else{
			$response["status"]			= false;
			$file = $this->upload->display_errors();
		}
		
		$response["message"]		= "";	
		$response["data"]			= $file;	
		json($response);
	}
}
