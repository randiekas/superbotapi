<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Toko extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->jwt				= cek_akses();
	}
	
	public function index($disetujui=0){
		$response["status"]		= true;
		$response["message"]	= "";

		$response["data"]		= $this->db
				->query("
					select 
						*
					from 
						toko
					where
						disetujui = ".$disetujui."
				")
				->result();
		json($response);
	}

	public function hapus($id=0){
		$response["status"]		= true;
		$response["message"]	= "";

		$response["status"]		= $this->db
				->query("
					delete
					from 
						toko
					where 
						id = ".$id."
				");
		json($response);
	}
	
	public function ubah(){
		$response["status"]		= true;
		$response["message"]	= "";

		$post 					= post_data();
		
		$data["disetujui"]		= $post->disetujui;
		
		$this->db->where("id", $post->id);
		$response["status"]		= $this->db->update("toko", $data);
		
		json($response);
	}

}
