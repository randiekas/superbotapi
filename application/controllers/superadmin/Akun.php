<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akun extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->jwt				= cek_akses();
	}
	
	public function index(){
		$response["status"]		= true;
		$response["message"]	= "";

		$response["data"]		= $this->db
				->query("
					select 
						*
					from 
						akun
				")
				->result();
		json($response);
	}

	public function hapus($id=0){
		$response["status"]		= true;
		$response["message"]	= "";

		$response["status"]		= $this->db
				->query("
					delete
					from 
						akun
					where 
						id = ".$id."
				");
		json($response);
	}

	public function simpan(){
		$response["status"]		= true;
		$response["message"]	= "";

		$post 					= (Object) post_data();
		
		$data["email"]			= $post->email;
		$data["password"]		= $post->password;
		$data["keterangan"]		= $post->keterangan;
		$data["tipe"]			= $post->tipe;
		$data["diubah"]			= date("Y-m-d H:i:s");
		
		if(@$post->id){
			$this->db->where("id", $post->id);
			$response["status"]		= $this->db->update("akun", $data);
		}else{
			$response["status"]		= $this->db->insert("akun", $data);
		}
		
		json($response);
	}

}
