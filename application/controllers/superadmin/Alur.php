<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alur extends CI_Controller {
	public function __construct(){
		parent::__construct();
		header( 'Access-Control-Allow-Origin: *' );
		if ( $_SERVER[ 'REQUEST_METHOD' ] == "OPTIONS" )
		{
			header( 'Access-Control-Allow-Credentials: true' );
			header( 'Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS' );
			header( 'Access-Control-Allow-Headers: ACCEPT, ORIGIN, X-REQUESTED-WITH, CONTENT-TYPE, AUTHORIZATION' );
			header( 'Access-Control-Max-Age: 86400' );
			header( 'Content-Length: 0' );
			header( 'Content-Type: text/plain' );
			exit ;
		}
	}

	public function index(){
		// setdata
		$response["status"]		= false;
		$response["message"]	= "";
		$response["data"]		= $this->db->get("alur")->result();
		
		json($response);
	}

	public function hapus($id){
		// setdata
		$response["status"]		= false;
		$response["message"]	= "Alur berhasil dihapus";

		$this->db->where("id", $id);
		$response["status"]		= $this->db->delete("alur");
		json($response);
	}

	public function pesan(){
		// setdata
		$response["status"]		= true;
		$response["message"]	= "";

		$post 					= (Object) post_data();

		$this->db->select("balasan, mode, opsi");

		if(isset($post->aksi)){
			$this->db->where("id", $post->tujuan);
		}else{
			$this->db->like("katakunci", $post->katakunci);
		}
		
		$result					= $this->db->get("alur", 1, 0);
		if($result->num_rows()==0){
			$response["data"]["balasan"]	= json_encode(array("Maaf saya tidak mengerti yang kamu maksud
												coba pilih salah satu menu berikut ini ?
												1. jadwal [sd, smp, sma]
												2. belajar
												3. pengumuman"));
			$response["data"]["mode"]		= "teks";
			$response["data"]["opsi"]		= json_encode(array());
		}else{
			$response["data"]		= $result->last_row();
		}
		json($response);
	}

	public function simpan(){
		// setdata
		$response["status"]		= true;
		$response["message"]	= "Alur berhasil disimpan";

		$post 					= (Object) post_data();
		
		if($post->id!=""){
			$this->db->where("id", $post->id);
			$response["status"]		= $this->db->update("alur", $post);
		}else{
			$post->dibuat			= date("Y-m-d H:i:s");
			$response["status"]		= $this->db->insert("alur", $post);
		}
		
		
		json($response);
	}

}
