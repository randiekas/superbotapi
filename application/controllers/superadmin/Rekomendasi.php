<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekomendasi extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->jwt				= cek_akses();
	}
	
	public function index(){
		$response["status"]		= true;
		$response["message"]	= "";

		$response["data"]		= $this->db
				->query("
					select produk.* 
					from produk_rekomendasi
					left join produk
						on produk.id = produk_rekomendasi.id_produk
				")
				->result();
		json($response);
	}

	public function produk(){
		$response["status"]		= true;
		$response["message"]	= "";

		$response["data"]		= $this->db
				->query("
					select 
						produk.* 
					from produk
					left outer join produk_rekomendasi
						on produk.id = produk_rekomendasi.id_produk
					where
						produk_rekomendasi.id is null
				")
				->result();
		json($response);
	}

	public function hapus($id=0){
		$response["status"]		= true;
		$response["message"]	= "";

		$response["status"]		= $this->db
				->query("
					delete
					from produk_rekomendasi
					where 
						id_produk = ".$id."
				");
		json($response);
	}

	public function tambah(){
		$response["status"]		= true;
		$response["message"]	= "";

		$post 					= post_data();
		
		$data["id_produk"]		= $post->id_produk;
		
		if(@$post->id){
			$this->db->where("id", $post->id);
			$response["status"]		= $this->db->update("produk_rekomendasi", $data);
		}else{
			$response["status"]		= $this->db->insert("produk_rekomendasi", $data);
		}
		
		json($response);
	}

}
